Django==1.6.5
South==0.8.4
argparse==1.2.1
wsgiref==0.1.2
Pillow==2.4.0
django-widget-tweaks==1.3
Unidecode==0.04.16
python-slugify==0.0.7