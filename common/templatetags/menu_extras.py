from django import template
from articles.models import Category


register = template.Library()

VISIBLE_CATEGORIES_COUNT = 7


@register.inclusion_tag(file_name='base/shared/menu.html')
def make_menu():
    categories = Category.objects.filter(active=True).order_by('order').all()
    visible_categories = categories[:VISIBLE_CATEGORIES_COUNT]
    hidden_categories = categories[VISIBLE_CATEGORIES_COUNT:]
    return {'visible_categories': visible_categories, 'hidden_categories': hidden_categories}