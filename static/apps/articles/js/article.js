function set_article_rating(choice) {
    var csrf_token = $("input[name=csrfmiddlewaretoken]").val();
    var article_id = $("#article_id").val();

    var like_tag = $('#article_like_button');
    var dislike_tag = $('#article_dislike_button');
    var rating_tag = $('#article_rating');
    var current_rating = parseInt(rating_tag.html());

    if ($("#is_user_authenticated").val() == "True") {
        switch (choice) {
            case "like":
                if (dislike_tag.hasClass('choice')) {
                    dislike_tag.removeClass('choice');
                    current_rating += 1;
                } else if (!like_tag.hasClass('choice')) {
                    like_tag.addClass('choice');
                    current_rating += 1;
                }
                if (current_rating > 0)
                    current_rating = '+' + current_rating;
                rating_tag.html(current_rating);
                break;
            case "dislike":
                if (like_tag.hasClass('choice')) {
                    like_tag.removeClass('choice');
                    current_rating -= 1;
                } else if (!dislike_tag.hasClass('choice')) {
                    dislike_tag.addClass('choice');
                    current_rating -= 1;
                }
                if (current_rating > 0)
                    current_rating = '+' + current_rating;
                rating_tag.html(current_rating);
                break;
        }
    }

    $.ajax({
        url: "/news/set_article_rating",
        type: "post",
        data: {
            'csrfmiddlewaretoken': csrf_token,
            "article_id": article_id,
            "choice": choice
        },
        success: function (response_date) {
            $.each(response_date['actions'], function (i, action) {
                if (action == "add_like_class") {
                    like_tag.addClass('choice');
                    rating_tag.addClass('top');
                } else if (action == "add_dislike_class") {
                    dislike_tag.addClass('choice');
                    rating_tag.addClass('low');
                } else if (action == "remove_like_class") {
                    like_tag.removeClass('choice');
                    rating_tag.removeClass('top');
                } else if (action == "remove_dislike_class") {
                    dislike_tag.removeClass('choice');
                    rating_tag.removeClass('low');
                } else if (action == "update_rating") {
                    var rating = response_date['rating'];
                    if (rating > 0)
                        rating = '+' + rating;
                    rating_tag.html(rating);
                } else if (action == "show_message") {
                    var span = $("#article_rating_message");
                    span.show();
                    span.text(response_date['message']);
                } else if (action == "set_is_user_authenticated_to_false") {
                    $("#is_user_authenticated").val('False');
                }
            });
        }
    });
}