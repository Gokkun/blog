from __future__ import unicode_literals

from django.shortcuts import render, render_to_response, redirect, \
    get_object_or_404, HttpResponse
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseForbidden
from django.template.defaultfilters import slugify
from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.csrf import csrf_protect
from django.db.models import Q

from .models import Article, Category, Tag, Comment
from .forms import SearchForm
from gallery.models import Image
from datetime import datetime, timedelta
import json
from unidecode import unidecode
from django.contrib.auth.models import User

ARTICLES_PER_PAGE = 5


def tags(request, tag_slug):
    print tag_slug
    tag = get_object_or_404(Tag, slug=tag_slug)
    news = tag.articles.order_by('-pub_date')[:32]
    context = {
        'news': news,
        'tag_name': tag.name
    }
    return render_to_response('apps/articles/tag_news_grid.html', RequestContext(request, context))


def view_tags(request):
    tag_list = Tag.objects.exclude(articles__isnull=True).order_by('name').all()
    tags_conteiner = []

    letter = tag_list[0].name[0]
    tag_list_in_container = []
    for tag in tag_list:
        if tag.name[0] != letter:
            tags_conteiner.append((letter, tag_list_in_container))
            letter = tag.name[0]
            tag_list_in_container = []
        tag_list_in_container.append(tag)

    context = {
        'tags_conteiner': tags_conteiner,
    }
    return render_to_response('apps/articles/tag_list.html', RequestContext(request, context))


def search(request):
    def clean_tags(s):
        s = s.split(',')
        tmp = []
        for tag in s:
            tag = tag.strip()
            if tag != '':
                tmp.append(tag)
        return tmp

    page = int(request.GET.get('page', 1))
    path_param_search = request.GET.get('search', '')
    path_param_tags = request.GET.get('tags', '')
    path_param_regions = request.GET.get('regions', '')
    path_param_category = request.GET.get('category', '')
    path_param_order_by = request.GET.get('order_by', '')
    path_param_search_in = request.GET.get('search_in', 'everywhere')

    form = SearchForm(request.GET)

    form.is_valid()

    try:
        articles = form.cleaned_data['category'].article_set
    except KeyError:
        articles = Article.objects

    articles = articles.filter(state=Article.ACTIVE)
    if not request.user.is_authenticated():
        articles = articles.exclude(access_mask=Article.VISIBLE_REGISTERED)

    tag_list = []
    region_list = []

    for tag in clean_tags(path_param_tags):
        tag_instance = Tag.objects.get_or_create(name=tag)[0]
        tag_list.append(tag_instance.pk)
    for region in clean_tags(path_param_regions):
        tag_instance = Tag.objects.get_or_create(name=region)[0]
        region_list.append(tag_instance.pk)

    if len(tag_list) > 0:
        articles = articles.filter(tags__in=tag_list)
    if len(region_list) > 0:
        articles = articles.filter(regions__in=region_list)

    if path_param_search:
        if form.cleaned_data['search_in'] == 'content':
            articles = articles.filter(full_text__contains=path_param_search)
        if form.cleaned_data['search_in'] == 'title':
            articles = articles.filter(title__contains=path_param_search)
        if form.cleaned_data['search_in'] == 'title':
            articles = articles.filter(Q(full_text__contains=path_param_search) | Q(title__contains=path_param_search))

    articles = articles.distinct()

    if form.cleaned_data['order_by'] == 'pub_date':
        articles = articles.order_by('-pub_date')
    elif form.cleaned_data['order_by'] == 'most_view':
        articles = articles.order_by('-views')
    elif form.cleaned_data['order_by'] == 'popularity':
        article_list_with_rating = [(x.rating(), x) for x in articles.all()]
        articles = [x[1] for x in sorted(article_list_with_rating, key=lambda x: x[0], reverse=True)]

    paginator = Paginator(articles, ARTICLES_PER_PAGE)
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page(1)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    context = {
        'form': form,
        'news': articles,
        'path_config': {
            'search': path_param_search,
            'tags': path_param_tags,
            'regions': path_param_regions,
            'category': path_param_category,
            'order_by': path_param_order_by,
            'search_in': path_param_search_in,
            'absolute_path_without_page': '{}?search={}&tags={}&regions={}&category={}&order_by={}&search_in={}'.
                format(request.path, path_param_search, path_param_tags, path_param_regions, path_param_category,
                       path_param_order_by, path_param_search_in)
        },
        'pagination': {}
    }
    context['pagination']['has_prev_button'] = True if page != 1 else False
    context['pagination']['has_next_button'] = True if page != paginator.num_pages else False
    context['pagination']['prev_pages'] = [page - i for i in (4, 3, 2, 1) if page - i >= 1]
    context['pagination']['next_pages'] = [page + i for i in (1, 2, 3, 4) if page + i <= paginator.num_pages]
    return render_to_response('apps/articles/search.html', RequestContext(request, context))


def view_category(request, category_slug):
    page = int(request.GET.get('page', 1))
    sorting_filter = request.GET.get('sorting', 'latest')
    time_filter = request.GET.get('time', 'day')

    category = get_object_or_404(Category, slug=category_slug, active=True)
    articles = category.article_set

    if time_filter == "all":
        pass  # for filter query
    elif time_filter == 'day':
        end_date = datetime.now()
        start_date = end_date - timedelta(days=1)
        articles = articles.filter(pub_date__range=(start_date, end_date))
    elif time_filter == 'week':
        end_date = datetime.now()
        start_date = end_date - timedelta(weeks=1)
        articles = articles.filter(pub_date__range=(start_date, end_date))
    elif time_filter == 'month':
        end_date = datetime.now()
        start_date = end_date - timedelta(days=31)
        articles = articles.filter(pub_date__range=(start_date, end_date))
    else:
        return Http404()

    if sorting_filter == "latest":
        articles = articles.order_by('-pub_date')
    elif sorting_filter == "important":
        articles = articles.filter(important=True).order_by('-pub_date')
    elif sorting_filter == "most_views":
        articles = articles.order_by('-views')
    elif sorting_filter == "popular":
        article_list_with_rating = [(x.rating(), x) for x in articles.all()]
        articles = [x[1] for x in sorted(article_list_with_rating, key=lambda x: x[0], reverse=True)]
    else:
        return Http404()

    paginator = Paginator(articles, ARTICLES_PER_PAGE)
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page(1)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    context = {
        'news': articles,
        'category_name': category.name,
        'active_menu_item': category.slug,
        'path_config': {
            'sorting': sorting_filter,
            'time': time_filter,
            'page': page,
            'absolute_path_without_page': '{}?sorting={}&time={}'.format(request.path, sorting_filter, time_filter)
        },
        'pagination': {}
    }
    context['pagination']['has_prev_button'] = True if page != 1 else False
    context['pagination']['has_next_button'] = True if page != paginator.num_pages else False
    context['pagination']['prev_pages'] = [page - i for i in (4, 3, 2, 1) if page - i >= 1]
    context['pagination']['next_pages'] = [page + i for i in (1, 2, 3, 4) if page + i <= paginator.num_pages]

    return render_to_response('apps/articles/article_list.html', RequestContext(request, context))


def home_page(request):
    end_date = datetime.now()
    start_date = end_date - timedelta(days=1)

    # articles_per_last_day = Article.objects.filter(pub_date__range=(start_date, end_date)).all()
    articles_per_last_day = Article.objects.all()

    articles_important = articles_per_last_day.filter(important=True).order_by('-pub_date')
    articles_important_fixed = articles_important[:4]
    articles_important = articles_important[4:10]

    articles_per_last_day.exclude()

    articles_most_views = articles_per_last_day.order_by('-views')[:5]

    article_list_with_rating = [(x.rating(), x) for x in articles_per_last_day]
    articles_popular = [x[1] for x in sorted(article_list_with_rating, key=lambda x: x[0]) if x[0] >= 1]

    categories = [x for x in Category.objects.order_by('-order') if x.article_set.count() > 0]
    categories = sorted(categories, key=lambda x: x.article_set.count)

    categories_col1 = categories[::3]
    categories_col2 = categories[1::3]
    categories_col3 = categories[2::3]

    context = {
        'active_menu_item': 'home',
        'articles_important_fixed': articles_important_fixed,
        'articles_important': articles_important,
        'articles_most_views': articles_most_views,
        'articles_popular': articles_popular,
        'categories_col1': categories_col1,
        'categories_col2': categories_col2,
        'categories_col3': categories_col3
    }
    return render_to_response('apps/articles/home.html', RequestContext(request, context))


def full_article(request, article_slug):
    comments_on_page = 10
    article = get_object_or_404(Article, slug=article_slug)
    if article.state == Article.ACTIVE:
        if not request.user.is_authenticated() and \
                not article.access_mask == Article.VISIBLE_ALL:
            return redirect(reverse('login'))

        article.views += 1
        article.save()

        # images = ArticleImage.objects.filter(article=article)
        context = {}
        context['article'] = article
        # context['images'] = images

        comments = article.comment_set.all().order_by('-pub_date')
        if comments.count() > comments_on_page:
            context['comments_last_page'] = False
        else:
            context['comments_last_page'] = True

        # comments|last doesn't support QuerySet(on article.html)
        context['comments'] = list(comments[:comments_on_page])

        # Saving latest articles visited by user
        try:
            last2 = request.session['last2']
        except KeyError:
            last2 = None
        try:
            last1 = request.session['last1']
        except KeyError:
            last1 = None
        request.session['last3'] = last2
        request.session['last2'] = last1
        request.session['last1'] = article.id

        # Providing link to previous page
        try:
            prev_category = request.session['previous_category']
            prev_page = request.session['previous_page']
        except KeyError:
            prev_category = 'latest'
            prev_page = 1
        context['prev_page'] = prev_page
        context['active_menu_item'] = article.category.slug
        context['prev_category'] = prev_category if prev_category else 'latest'

        return render_to_response('apps/articles/article.html',
                                  RequestContext(request, context))
    else:
        raise Http404('Article is not active')


@csrf_protect
@login_required
def edit_article(request):
    def clean_tags(s):
        s = s.split(',')
        tmp = []
        for tag in s:
            tag = tag.strip()
            if tag != '':
                tmp.append(tag)
        return tmp

    context = {}

    if request.is_ajax():
        response_data = {'actions': [], 'set_fields': {}}
        article_id = None

        if len(request.FILES) > 0:
            try:
                article_id = request.POST['article_id']
                article = Article.objects.get(pk=article_id)

                image = Image(image_source=request.FILES['article_main_image'])
                image.save()

                article.main_image = image
                article.save()

                response_data['actions'].append('show_message')
                response_data['message'] = 'Image upload'
            except ObjectDoesNotExist:
                response_data['actions'].append('show_error_message')
                response_data['message'] = 'Article with id = {} does not exist'.format(article_id)
            except Exception as e:
                response_data['actions'].append('show_error_message')
                response_data['message'] = e.message

            return HttpResponse(json.dumps(response_data, cls=DjangoJSONEncoder), content_type="application/json")

        try:
            article_id = request.POST['article_id']
            article_title = request.POST['article_title']
            article_description = request.POST['article_description']
            article_category_id = request.POST['article_category']
            article_important = request.POST['article_important']
            article_access_mask = request.POST['article_access_mask']
            article_state = request.POST['article_state']
            article_content = request.POST['article_content']
            article_tags = request.POST['article_tags']
            article_region = request.POST['article_region']

            if article_state == 'ACT':
                article_pub_date = datetime.now()
            else:
                article_pub_date = None

            article_tags = clean_tags(article_tags)
            article_region = clean_tags(article_region)

            article = None

            if article_id:
                try:
                    article = Article.objects.get(pk=article_id)
                    article_author_id = article.author.pk
                    if article_author_id != request.user.pk:
                        response_data['actions'].append('show_error_message')
                        response_data['message'] = 'You are not author current the article'
                        return HttpResponse(json.dumps(response_data), content_type="application/json")
                except:
                    response_data['actions'].append('show_error_message')
                    response_data['error_message'] = 'Article does not exist'
                    return HttpResponse(json.dumps(response_data), content_type="application/json")

            if not article:
                article = Article(
                    author=request.user,
                    title=article_title,
                    thumbnail=article_description,
                    full_text=article_content,
                    pub_date=article_pub_date,
                    creation_date=datetime.now(),
                    important=bool(article_important),
                    state=article_state,
                    access_mask=article_access_mask,
                    category=Category.objects.get(pk=article_category_id)
                )
                response_data['set_fields']['article_creation_date'] = article.creation_date.strftime("%d %B, %I:%M %p")
                response_data['set_fields']['article_pub_date'] = article_pub_date.strftime("%d %B, %I:%M %p")
            else:
                article.title = article_title
                article.thumbnail = article_description
                article.full_text = article_content
                article.important = bool(article_important)
                article.state = article_state
                article.access_mask = article_access_mask
                article.category = Category.objects.get(pk=article_category_id)
                if (article.pub_date and article.state != "ACT") or (not article.pub_date and article.state == "ACT"):
                    article.pub_date = article_pub_date
                    response_data['set_fields']['article_pub_date'] = article_pub_date.strftime(
                        "%d %B, %I:%M %p") if article_pub_date else ''

            article.save()

            for tag in article_tags:
                tag_instance = Tag.objects.get_or_create(name=tag)[0]
                article.tags.add(tag_instance)
            for region in article_region:
                tag_instance = Tag.objects.get_or_create(name=region)[0]
                article.regions.add(tag_instance)

            article.slug = slugify(unidecode(article.title[:40]) + "-" + str(article.id))

            article.save()

            response_data['set_fields']['article_id'] = str(article.pk)
            response_data['set_fields']['article_url'] = article.get_absolute_url()
            response_data['actions'].append('show_message')
            response_data['message'] = 'Article saved'
            return HttpResponse(json.dumps(response_data, cls=DjangoJSONEncoder), content_type="application/json")

        except Exception as e:
            response_data['actions'].append('show_error_message')
            response_data['message'] = e.message
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        article_id = request.GET.get('id')
        if article_id:
            try:
                article = Article.objects.get(pk=article_id)
                article_author_id = article.author.pk
                if article_author_id != request.user.pk:
                    return HttpResponseForbidden()

                context['article'] = article
                context['article_tags'] = ', '.join((tag.name for tag in article.tags.all()))
                context['article_region'] = ', '.join((tag.name for tag in article.regions.all()))
            except:
                return HttpResponseForbidden()

    context['categories'] = Category.objects.all()
    return render(request, 'apps/articles/edit_article.html', context)


@csrf_protect
@login_required
def delete_article(request):
    if request.is_ajax() and request.user.is_authenticated():
        response_data = {'actions': [], 'set_fields': {}}
        try:
            article_id = request.POST['article_id']
            article = Article.objects.get(pk=article_id)
            author_id = article.author.pk

            if author_id != request.user.pk:
                return HttpResponseForbidden

            article.delete()
            response_data['actions'].append('show_action_model')
            response_data['title'] = 'Success'
            response_data['message'] = 'Article deleted. What do you want to do next?'
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        except Exception as e:
            response_data['actions'].append('show_error_message')
            response_data['message'] = e.message
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        return HttpResponseForbidden


@csrf_protect
@login_required
def comment_add(request):
    if request.is_ajax():
        response_data = {'actions': [], 'fields': {}}
        try:
            article_id = request.POST['article_id']
            comment_text = request.POST['comment_text']
            try:
                article = Article.objects.get(pk=article_id)
            except ObjectDoesNotExist:
                response_data['actions'].append('show_error_message')
                response_data['error_message'] = 'Article does not exist'
                return HttpResponse(json.dumps(response_data), content_type="application/json")
            comment = Comment(
                author=request.user,
                article=article,
                pub_date=datetime.now(),
                text=comment_text
            )
            comment.save()
            response_data['comment'] = comment.as_dict(request.user)
            response_data['actions'].append('show_message')
            response_data['actions'].append('show_new_comment')
            response_data['message'] = 'Comment submitted'
            return HttpResponse(json.dumps(response_data, cls=DjangoJSONEncoder), content_type="application/json")

        except Exception as e:
            response_data['actions'].append('show_error_message')
            response_data['message'] = e
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        raise Http404


@csrf_protect
@login_required
def comment_delete(request):
    if request.is_ajax():
        response_data = {'actions': [], 'fields': {}}
        try:
            comment_id = request.POST['comment_id']
            try:
                comment = Comment.objects.get(pk=comment_id)
            except ObjectDoesNotExist:
                response_data['actions'].append('show_alert')
                response_data['alert_message'] = 'Comment does not exist'
                return HttpResponse(json.dumps(response_data), content_type="application/json")
            comment.delete()
            response_data['fields']['comment_id'] = comment_id
            response_data['actions'].append('delete_comment')
            return HttpResponse(json.dumps(response_data, cls=DjangoJSONEncoder), content_type="application/json")

        except Exception as e:
            response_data['actions'].append('show_alert')
            response_data['alert_message'] = e
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        raise Http404


@csrf_protect
@login_required
def comment_update(request):
    if request.is_ajax():
        response_data = {'actions': [], 'fields': {}}
        try:
            comment_id = request.POST['comment_id']
            comment_text = request.POST['comment_text']
            try:
                comment = Comment.objects.get(pk=comment_id)
            except ObjectDoesNotExist:
                response_data['actions'].append('show_alert')
                response_data['alert_message'] = 'Comment does not exist'
                return HttpResponse(json.dumps(response_data), content_type="application/json")
            if comment.author != request.user:
                response_data['actions'].append('show_alert')
                response_data['actions'].append('close_edit_form')
                response_data['alert_message'] = "You aren't author"
                return HttpResponse(json.dumps(response_data), content_type="application/json")

            comment.text = comment_text
            comment.save()
            response_data['fields']['comment_id'] = comment_id
            response_data['actions'].append('update_comment')
            return HttpResponse(json.dumps(response_data, cls=DjangoJSONEncoder), content_type="application/json")

        except Exception as e:
            response_data['actions'].append('show_alert')
            response_data['alert_message'] = e
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        raise Http404


@csrf_protect
def comment_next_page(request):
    comments_on_page = 10
    if request.is_ajax():
        response_data = {'actions': [], 'fields': {}}
        try:
            article_id = request.POST['article_id']
            last_id = int(request.POST['last_id'])
            try:
                article = Article.objects.get(pk=article_id)
            except ObjectDoesNotExist:
                response_data['actions'].append('show_alert')
                response_data['alert_message'] = 'Article does not exist'
                return HttpResponse(json.dumps(response_data), content_type="application/json")

            comments = article.comment_set.filter(id__lt=last_id).order_by('-pub_date')

            if comments.count() > comments_on_page:
                response_data['actions'].append('show_comm_next_page_button')
            comments = comments[:comments_on_page]

            response_data['fields']['comments'] = [x.as_dict(request.user) for x in comments]
            response_data['actions'].append('add_comments')
            return HttpResponse(json.dumps(response_data, cls=DjangoJSONEncoder), content_type="application/json")

        except Exception as e:
            response_data['actions'].append('show_alert')
            response_data['alert_message'] = e
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        raise Http404


def set_comment_rating(request):
    if request.is_ajax():
        response_data = {'actions': []}

        try:
            comment_id = request.POST['comment_id']
            choice = request.POST['choice']
            comment = Comment.objects.get(pk=comment_id)
        except (ObjectDoesNotExist, ValueError):
            response_data['actions'].append('remove_like_class')
            response_data['actions'].append('remove_dislike_class')
            response_data['actions'].append('update_rating')
            response_data['rating'] = 0
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        if not request.user.is_authenticated():
            response_data['actions'].append('set_is_user_authenticated_to_false')
            response_data['actions'].append('show_message')
            response_data['actions'].append('remove_like_class')
            response_data['actions'].append('remove_dislike_class')
            response_data['actions'].append('update_rating')
            response_data['rating'] = comment.rating()
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        if choice == 'like':
            if request.user in comment.users_liked.all():
                response_data['actions'].append('none')
                return HttpResponse(json.dumps(response_data), content_type="application/json")

            if request.user in comment.users_disliked.all():
                comment.users_disliked.remove(request.user)
                rating = comment.rating()

                response_data['actions'].append('remove_dislike_class')
                response_data['actions'].append('update_rating')
                response_data['rating'] = rating
                return HttpResponse(json.dumps(response_data), content_type="application/json")

            comment.users_liked.add(request.user)
            rating = comment.rating()

            response_data['actions'].append('add_like_class')
            response_data['actions'].append('update_rating')
            response_data['rating'] = rating
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        if choice == 'dislike':
            if request.user in comment.users_disliked.all():
                response_data['actions'].append('none')
                return HttpResponse(json.dumps(response_data), content_type="application/json")

            if request.user in comment.users_liked.all():
                comment.users_liked.remove(request.user)
                response_data['actions'].append('remove_like_class')
            else:
                comment.users_disliked.add(request.user)
                response_data['actions'].append('add_dislike_class')

        response_data['actions'].append('update_rating')
        response_data['rating'] = comment.rating()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


def set_article_rating(request):
    if request.is_ajax():
        response_data = {'actions': []}

        try:
            article_id = request.POST['article_id']
            choice = request.POST['choice']
            article = Article.objects.get(pk=article_id)
        except (ObjectDoesNotExist, ValueError):
            response_data['actions'].append('remove_like_class')
            response_data['actions'].append('remove_dislike_class')
            response_data['actions'].append('update_rating')
            response_data['rating'] = 0
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        if not request.user.is_authenticated():
            response_data['actions'].append('set_is_user_authenticated_to_false')
            response_data['actions'].append('show_message')
            response_data['actions'].append('remove_like_class')
            response_data['actions'].append('remove_dislike_class')
            response_data['actions'].append('update_rating')
            response_data['rating'] = article.rating()
            response_data['message'] = 'You are not authorized.<a href="/login?next=' +{{ request.path }}+'">Log in</a> to rate comments'
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        if request.user.pk == article.author.pk:
            response_data['actions'].append('set_is_user_authenticated_to_false')
            response_data['actions'].append('show_message')
            response_data['actions'].append('remove_like_class')
            response_data['actions'].append('remove_dislike_class')
            response_data['actions'].append('update_rating')
            response_data['rating'] = article.rating()
            response_data['message'] = 'You cannot rate own article.'
            return HttpResponse(json.dumps(response_data), content_type="application/json")


        if choice == 'like':
            if request.user in article.users_liked.all():
                response_data['actions'].append('none')
                return HttpResponse(json.dumps(response_data), content_type="application/json")

            if request.user in article.users_disliked.all():
                article.users_disliked.remove(request.user)
                rating = article.rating()

                response_data['actions'].append('remove_dislike_class')
                response_data['actions'].append('update_rating')
                response_data['rating'] = rating
                return HttpResponse(json.dumps(response_data), content_type="application/json")

            article.users_liked.add(request.user)
            rating = article.rating()

            response_data['actions'].append('add_like_class')
            response_data['actions'].append('update_rating')
            response_data['rating'] = rating
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        if choice == 'dislike':
            if request.user in article.users_disliked.all():
                response_data['actions'].append('none')
                return HttpResponse(json.dumps(response_data), content_type="application/json")

            if request.user in article.users_liked.all():
                article.users_liked.remove(request.user)
                response_data['actions'].append('remove_like_class')
            else:
                article.users_disliked.add(request.user)
                response_data['actions'].append('add_dislike_class')

        response_data['actions'].append('update_rating')
        response_data['rating'] = article.rating()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


def user_articles(request, user_name):
    page = int(request.GET.get('page', 1))
    sorting_filter = request.GET.get('sorting', 'latest')
    time_filter = request.GET.get('time', 'all')

    user = get_object_or_404(User, username=user_name)
    articles = user.article_set

    if not request.user.is_authenticated():
        articles = articles.exclude(access_mask=Article.VISIBLE_REGISTERED)

    if time_filter == "all":
        pass  # for filter query
    elif time_filter == 'day':
        end_date = datetime.now()
        start_date = end_date - timedelta(days=1)
        articles = articles.filter(pub_date__range=(start_date, end_date))
    elif time_filter == 'week':
        end_date = datetime.now()
        start_date = end_date - timedelta(weeks=1)
        articles = articles.filter(pub_date__range=(start_date, end_date))
    elif time_filter == 'month':
        end_date = datetime.now()
        start_date = end_date - timedelta(days=31)
        articles = articles.filter(pub_date__range=(start_date, end_date))
    else:
        return Http404()

    if sorting_filter == "latest":
        articles = articles.order_by('-pub_date')
    elif sorting_filter == "important":
        articles = articles.filter(important=True).order_by('-pub_date')
    elif sorting_filter == "most_views":
        articles = articles.order_by('-views')
    elif sorting_filter == "popular":
        article_list_with_rating = [(x.rating(), x) for x in articles.all()]
        articles = [x[1] for x in sorted(article_list_with_rating, key=lambda x: x[0], reverse=True)]
    else:
        return Http404()

    paginator = Paginator(articles, ARTICLES_PER_PAGE)
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page(1)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    context = {
        'news': articles,
        'path_config': {
            'sorting': sorting_filter,
            'time': time_filter,
            'page': page,
            'absolute_path_without_page': '{}?sorting={}&time={}'.format(request.path, sorting_filter, time_filter)
        },
        'pagination': {}
    }
    context['pagination']['has_prev_button'] = True if page != 1 else False
    context['pagination']['has_next_button'] = True if page != paginator.num_pages else False
    context['pagination']['prev_pages'] = [page - i for i in (4, 3, 2, 1) if page - i >= 1]
    context['pagination']['next_pages'] = [page + i for i in (1, 2, 3, 4) if page + i <= paginator.num_pages]

    return render_to_response('apps/articles/article_list.html', RequestContext(request, context))