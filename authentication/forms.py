from django import forms

class SigninForm(forms.Form):
    username = forms.CharField(max_length=80, required=True)
    password = forms.CharField(max_length=80, required=True, widget=forms.PasswordInput)