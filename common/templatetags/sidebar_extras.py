from django import template
from articles.models import Article

register = template.Library()

ARTICLES_IN_SIDEBAR_MAX_COUNT = 10


@register.inclusion_tag(takes_context=True, file_name='base/shared/sidebar.html')
def sidebar_popular_news(context):
    articles = Article.objects.filter(state=Article.ACTIVE)
    if not context['user'].is_authenticated():
        articles = articles.exclude(access_mask=Article.VISIBLE_REGISTERED)

    article_list_with_rating = [(x.rating(), x) for x in articles]
    articles = [x[1] for x in sorted(article_list_with_rating, key=lambda x: x[0], reverse=True)]
    articles = articles[:ARTICLES_IN_SIDEBAR_MAX_COUNT]
    return{'articles': articles, 'sidebar_name': 'popular news'}


@register.inclusion_tag(takes_context=True, file_name='base/shared/sidebar.html')
def sidebar_important_news(context):
    articles = Article.objects.filter(important=True).filter(state=Article.ACTIVE).order_by('-pub_date')
    if not context['user'].is_authenticated():
        articles = articles.exclude(access_mask=Article.VISIBLE_REGISTERED)

    articles = articles[:ARTICLES_IN_SIDEBAR_MAX_COUNT]
    return {'articles': articles, 'sidebar_name': 'important news'}


@register.inclusion_tag(takes_context=True, file_name='base/shared/sidebar.html')
def sidebar_latest_news(context):
    articles = Article.objects.filter(state=Article.ACTIVE).order_by('-pub_date')
    if not context['user'].is_authenticated():
        articles = articles.exclude(access_mask=Article.VISIBLE_REGISTERED)

    articles = articles[:ARTICLES_IN_SIDEBAR_MAX_COUNT]
    return {'articles': articles, 'sidebar_name': 'latest news'}


@register.inclusion_tag(takes_context=True, file_name='base/shared/sidebar.html')
def sidebar_related_news(context):
    current_article = context['article']
    tag_list = current_article.tags.values_list('pk')
    tag_list = [x[0] for x in tag_list]

    category = current_article.category
    category_articles = category.article_set.filter(state=Article.ACTIVE)
    if not context['user'].is_authenticated():
        category_articles = category_articles.exclude(access_mask=Article.VISIBLE_REGISTERED)

    article_list_with_overlap = []

    for article in category_articles:
        if article.pk != current_article.pk:
            overlap = article.tags.filter(pk__in=tag_list).count()
            article_list_with_overlap.append((overlap, article))

    article_list_with_overlap = sorted(article_list_with_overlap, key=lambda x: x[0], reverse=True)
    article = [x[1] for x in article_list_with_overlap[:ARTICLES_IN_SIDEBAR_MAX_COUNT]]
    return {'articles': article, 'sidebar_name': 'related news'}
