from django.contrib import admin
from django.forms import Textarea, NumberInput
from django.db import models

from .models import Article, Tag, Category, Comment
from .admin_additional import ActiveFilter

from django.contrib.admin.widgets import ManyToManyRawIdWidget

from gallery.models import Image
# admin.site.disable_action('delete_selected')


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('creation_date', 'title', 'author',
                    'important', 'category', 'views', 'state', 'access_mask', )
    list_display_links = ('title', )
    search_fields = ('title', 'author')
    raw_id_fields = ('author', 'main_image')
    list_filter = ('important', 'state', 'access_mask')
    fieldsets = (
        ('Main information', {
            'fields': ['author', 'title', 'slug',
                                         'thumbnail', 'full_text', 'main_image']}),
        (None, {'fields': ['important', 'creation_date', 'pub_date', 'views',
                           'state', 'access_mask']}),
        (None, {'fields': ['category', 'tags']}),
        ('Images', {
            'fields': ('images', )
        }),
    )
    filter_horizontal = ('tags', 'images')
    prepopulated_fields = {"slug": ("title",)}
    date_hierarchy = 'pub_date'
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 40})},
        models.ManyToManyField: {'widget': ManyToManyRawIdWidget(Image.objects, Article._meta.get_field('images').rel)},
    }
    list_editable = ('important', 'state', 'access_mask',)
    readonly_fields = ('views', 'creation_date', )
    radio_fields = {'state': admin.HORIZONTAL,
                    'access_mask': admin.HORIZONTAL}
    list_per_page = 10
    ordering = ['-creation_date', 'title']
    actions = ['delete_selected', ]
    save_on_top = True
    save_as = True

    #This feature will be available in Django 1.7
    # def view_on_site(self, obj):
    #     return reverse('articles:full_article', args=(obj.slug, ), )#kwargs={'article': obj})


class TagAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'usage', )
    list_display_links = ('name', )
    list_filter = ('type', )
    list_editable = ('type', )
    ordering = ('name', )
    fields = ('name', 'type', 'usage', )
    readonly_fields = ('usage', )
    actions = ('update_slugs', )

    def update_slugs(self, request, queryset):
        for obj in queryset:
            obj.save()
        self.message_user(request, "{} tags successfully updated.".format(len(queryset)))
    update_slugs.short_description = "Update slugs for tags"


class CommentAdmin(admin.ModelAdmin):
    list_display = ('article', 'author', 'text', 'pub_date', 'parent')
    list_display_links = ['article', 'text']
    search_fields = ['article', 'text']
    list_filter = ['pub_date']


#Following way of registration Model with custom ModelAdmin
# will work in Django 1.7.
#@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'meta_tags', 'active', 'order', ]
    list_display_links = ['name', ]
    search_fields = ['meta_tags', ]
    list_filter = [ActiveFilter, ]
    list_editable = ['order', ]
    fields = ['name', 'meta_tags', 'order', 'active', 'slug', ]
    actions = ['make_active', 'make_inactive', ]
    ordering = ['order', ]
    prepopulated_fields = {'slug': ('name',)}
    save_as = True
    formfield_overrides = {
        models.PositiveSmallIntegerField: {'widget': NumberInput},
    }

    def make_active(self, request, queryset):
        rows_updated = queryset.update(active=True)
        if rows_updated == 1:
            user_message = "1 category was activated"
        else:
            user_message = "{} categories was activated".format(rows_updated)
        self.message_user(request, user_message)
    make_active.short_description = "Activate selected categories"

    def make_inactive(self, request, queryset):
        rows_updated = queryset.update(active=False)
        if rows_updated == 1:
            user_message = "1 category was inactivated"
        else:
            user_message = "{} categories was inactivated".format(rows_updated)
        self.message_user(request, user_message)
    make_inactive.short_description = "Inactivate selected categories"

    #TODO: write proper function that change activity for selected categories.
    # def reverse_activity(self, request, queryset):
    #     for category in queryset:
    #         category.update(active=!category.active)
    #     rows_updated = len(queryset)
    #     if rows_updated == 1:
    #         user_message = "1 category was reversed"
    #     else:
    #         user_message = "{} categories was reversed".format(rows_updated)
    #     self.message_user(request, user_message)
    # reverse_activity.short_description = \
    #     "Reverse activity of selected categories"

admin.site.register(Article, ArticleAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Category, CategoryAdmin)