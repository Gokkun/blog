from __future__ import unicode_literals

from django.shortcuts import render, redirect, get_object_or_404, HttpResponse, render_to_response
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm
from django.core.serializers.json import DjangoJSONEncoder
from django.http import Http404, HttpResponseForbidden, HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie

import json

from .forms import RegistrationForm, EditUserForm, EditProfileForm
from .models import Profile, Subscription
from .action import Action

from gallery.models import Image
from articles.models import Article, Comment


def create_user(request):
    if request.user.is_authenticated():
        return redirect(reverse('users:dashboard'))

    context = {}
    user = User()

    if request.method == 'POST':
        form = RegistrationForm(request.POST, instance=user)
        if form.is_valid():
            user.set_password(request.POST['password'])
            user.save()

            profile = Profile(user=user)
            profile.save()

            user = authenticate(username=request.POST['username'], password=request.POST['password'])
            login(request, user)
            return redirect(reverse('users:dashboard'))
    else:
        form = RegistrationForm(instance=user)

    context['form'] = form
    return render(request, 'apps/profiles/register.html', context)


@login_required
def home_page(request):
    images = Image.objects.filter(author=request.user).order_by('-created')[:30]
    articles_by_user = Article.objects.filter(author=request.user)
    active_articles_count = articles_by_user.filter(state=Article.ACTIVE,
                                                    category__active=True).count()
    articles_count = articles_by_user.count()
    comments_count = Comment.objects.filter(author=request.user).count()

    #Recent actions.
    articles = Article.objects.filter(author=request.user, state=Article.ACTIVE)
    recent_actions = []
    for article in articles:
        recent_actions.append(Action('Article "{0}" {1}'.format(article.title, 'created' if article.pub_date == article.creation_date else 'published'), article.pub_date))
    comments = Comment.objects.filter(author=request.user)
    for comment in comments:
        recent_actions.append(Action('Comment for article "{}" added'.format(comment.article.title), comment.pub_date))
    for image in images:
        recent_actions.append(Action('Image "{}" uploaded'.format(image.title), image.created))
    recent_actions.sort(cmp=lambda x, y: cmp(x.date, y.date), reverse=True)
    percentage = 40 if request.user.first_name and request.user.last_name \
        else (20 if request.user.first_name or request.user.last_name else 0)
    percentage += 20 if request.user.email else 0
    percentage += 10 if request.user.profile.birth_date else 0
    percentage += 10 if request.user.profile.phone_number else 0
    percentage += 10 if request.user.profile.skype else 0
    percentage += 10 if request.user.profile.activity else 0

    authors = Subscription.objects.filter(user=request.user).values_list('subscribe_to', flat=True).order_by('subscribe_to')
    subscriptions = Profile.objects.filter(user__in=authors).values('user__username', 'avatar', 'user__pk').order_by('user__username')
    followers = Subscription.objects.filter(subscribe_to=request.user).values_list('user', flat=True).order_by('user')
    followed_by = Profile.objects.filter(user__in=followers).values('user__username', 'avatar', 'user__pk').order_by('user__username')
    context = {'user': request.user,
               'images': images,
               'articles_count': articles_count,
               'active_articles_count': active_articles_count,
               'comments_count': comments_count,
               'recent_actions': recent_actions[:20],
               'percentage_profile': percentage,
               'followers': followed_by,
               'subscriptions': subscriptions,
               'edit_pane': True, }
    if request.method == 'POST':
        form = EditUserForm(request.POST)
        second_form = EditProfileForm(request.POST)
        password_form = PasswordChangeForm(user=request.user, data=request.POST)
        if password_form.has_changed():
            if password_form.is_valid():
                user = request.user
                user.set_password(password_form.cleaned_data['new_password1'])
                user.save()
                return HttpResponseRedirect(reverse('users:dashboard'))
            else:
                context['password_form'] = password_form
                context['password_pane'] = True
            user_form = EditUserForm(instance=request.user)
            context['user_form'] = user_form
            profile_form = EditProfileForm(instance=request.user.profile)
            context['profile_form'] = profile_form
            context['edit_pane'] = False
        else:
            if form.is_valid() and second_form.is_valid():
                user = request.user
                user.first_name = form.cleaned_data['first_name']
                user.last_name = form.cleaned_data['last_name']
                user.email = form.cleaned_data['email']
                user.save()
                profile = user.profile
                profile.birth_date = second_form.cleaned_data['birth_date']
                profile.activity = second_form.cleaned_data['activity']
                profile.skype = second_form.cleaned_data['skype']
                profile.phone_number = second_form.cleaned_data['phone_number']
                profile.save()
                return HttpResponseRedirect(reverse('users:dashboard'))
            else:
                context['user_form'] = form
                context['profile_form'] = second_form
                password_form = PasswordChangeForm(user=request.user)
                context['password_form'] = password_form
    else:
        user_form = EditUserForm(instance=request.user)
        context['user_form'] = user_form
        profile_form = EditProfileForm(instance=request.user.profile)
        context['profile_form'] = profile_form
        context['edit_pane'] = False
        password_form = PasswordChangeForm(user=request.user)
        context['password_form'] = password_form

    return render(request, 'apps/profiles/home.html', context)


@login_required
@ensure_csrf_cookie
def profile_page(request, user_name):
    user_profile = get_object_or_404(User, username=user_name)
    images = Image.objects.filter(author=user_profile).order_by('-created')[:30]
    articles_by_user = Article.objects.filter(author=user_profile)
    active_articles_count = articles_by_user.filter(state=Article.ACTIVE,
                                                    category__active=True).count()
    articles_count = articles_by_user.count()
    comments_count = Comment.objects.filter(author=user_profile).count()

    authors = Subscription.objects.filter(user=user_profile).values_list('subscribe_to', flat=True).order_by('subscribe_to')
    subscriptions = Profile.objects.filter(user__in=authors).values('user__username', 'avatar', 'user__pk').order_by('user__username')
    followers = Subscription.objects.filter(subscribe_to=user_profile).values_list('user', flat=True).order_by('user')
    followed_by = Profile.objects.filter(user__in=followers).values('user__username', 'avatar', 'user__pk').order_by('user__username')
    art_votes = user_profile.article_users_liked.count() + \
                user_profile.article_users_disliked.count()
    com_votes = user_profile.comment_users_liked.count() + \
                user_profile.comment_users_disliked.count()
    recent_articles = Article.objects.filter(author=user_profile, state=Article.ACTIVE).order_by('-pub_date')[:10]
    recent_comments = Comment.objects.filter(author=user_profile).order_by('-pub_date')[:10]
    context = {'user': user_profile,
               'logged_user': request.user,
               'images': images,
               'articles_count': articles_count,
               'active_articles_count': active_articles_count,
               'comments_count': comments_count,
               'subscriptions': subscriptions,
               'followers': followed_by,
               'articles_votes': art_votes,
               'comments_votes': com_votes,
               'recent_articles': recent_articles,
               'recent_comments': recent_comments, }
    return render(request, 'apps/profiles/profile_page.html', context)


@csrf_protect
@ensure_csrf_cookie
def subscribe(request):
    if request.is_ajax():
        if request.POST['place'] == 'profile_page':
            author = get_object_or_404(User, id=request.POST['author_id'])
            user = get_object_or_404(User, id=request.POST['user_id'])
        else:
            author = get_object_or_404(Article, id=request.POST['article_id']).author
            user = get_object_or_404(User, id=request.POST['user_id'])
        action = request.POST['action']
        if action == 'subscribe':
            try:
                Subscription.objects.get(user=user, subscribe_to=author)
            except ObjectDoesNotExist:
                subscription = Subscription(user=user, subscribe_to=author)
                subscription.save()
                pass
        elif action == 'unsubscribe':
            try:
                sub = Subscription.objects.get(user=user, subscribe_to=author)
            except ObjectDoesNotExist:
                pass
            sub.delete()
        return HttpResponse(json.dumps({}, cls=DjangoJSONEncoder),
                            content_type="application/json")
    else:
        raise HttpResponseForbidden


@csrf_protect
@ensure_csrf_cookie
def change_subscr(request):
    if request.is_ajax():
        from_user = request.POST['from_user']
        to_user = request.POST['to_user']
        try:
            subs = Subscription.objects.get(user__pk=from_user,
                                            subscribe_to__pk=to_user)
            subs.delete()
            response = {'action': 'removed'}
        except ObjectDoesNotExist:
            user = get_object_or_404(User, pk=from_user)
            sub_to = get_object_or_404(User, pk=to_user)
            subs = Subscription(user=user, subscribe_to=sub_to)
            subs.save()
            response = {'action': 'added'}
            return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder),
                                content_type="application/json")
        return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder),
                            content_type="application/json")
    else:
        raise HttpResponseForbidden
