from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Profile
#
# class UserProfileAdmin(admin.ModelAdmin):
#     list_display = ('user', )
#     raw_id_fields = ('user', )
#     fields = ('user', 'phone_number', 'birth_date', 'activity')
#
#
# admin.site.register(UserProfile, UserProfileAdmin)


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
#    verbose_name_plural = 'profile'


class UserAdmin(UserAdmin):
    inlines = (ProfileInline, )

admin.site.unregister(User)
admin.site.register(User, UserAdmin)